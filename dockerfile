FROM alpine

VOLUME /src
WORKDIR /src

RUN apk add --no-cache postgresql-client 

ENTRYPOINT []
